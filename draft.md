1 => Notifications (to read)
2 => Todo list (in progress)
3 => Calendar (done)


At a notification reception, you can immediatly acknowledge and archive it.
Also you can provide an estimated duration to complete it, and an estimated timeslot to complete it to transform it to one _or more_ flexible tasks.
also the time slot can be precisely fixed to allocate it in your calendar

Instead of providing a timeslot, you can push it onto a flexible list, to read later (pocket and so on)
.












Focus aims to be the articulation between the notifications and a timetable.
That is the application that allows you to manage your incoming tasks, news, mail, conversation and so on, and help you organize these tasks into your calendar.

Lists of postponable cards
Cards are bundled together in list : mails, messenging ...
They are working lists, that is a bundle of task to do one after the other.
Lists should be few (mails, messenging, reminders, social ...).

Lists are like folders, or tags.
Lists have
- an "urgency" or "expiration" setting to encompass todo lists and others (daily, and longer (to read list))

Each card correspond to a "notification" or a manual task/reminder.
Cards have
- a "flexibility" setting. The goal is to keep the rigid aspect of a calendar for some "tasks", and arrange the other tasks flexibly around.
  - for fixed cards, a "reminder" setting, to know how long before the beginning, to push the card in the list (to be aware of it)
- an acknowledgement status : to read (unaware), to do (aware but not completed yet), completed (task is done, and should be archived)
- an estimated completition durations associated (~5mn to read mails)
- a real completion duration associated, for statistics, and improve the estimation
- an "urgency" or "expiration" setting (by default, inherited from the list)
You can drag cards from one list to the other, this operation creates a new task (a card) in the new list, with the old cards attached.

There are two views :
- by default, it displays only the most urgent cards to focus on the current activity.
It intertwin cards of all the lists.
But if you want to do something else, you can postpone it to later (end of the list, next item on the list, precise time / date)
- And there is a birds-eye view that looks like a calendar, to display all the lists.

Inbox asks for the "available" hours, and "unavailable" hours.
And later automatically infers them.

- At the beginning of the day, it asks you some questions (about your planning, your motivation, your health, and so on ... maybe to determine the work to do and the pace for today)
- At the end of the day, it asks you to do a brief summary of the day, and more question if you want, and shows you all the cards you accomplished.
- During your day, it proposes you to take some rest frome time to time.


Timezone understanding


a portal to other applications ?
-> You want to read pocket articles, by clicking the corresponding cards, it opens the pocket app so that you can read them (and at the same time, it log your activity)
The danger, is that without using this portal, the activity is not logged, and by using this portal, you create a bottleneck of innovation.




- Card -> Todo / Notification / Reminder ...
- Threads -> a tree of cards linked with each other logically.
- Timeslot -> a card is allocated one or several timeslot in the calendar.
