export default {
  // Elements
  'list': 'list',
  'calendar': 'calendar',
  'pending': 'inbox',
  'scheduled': 'clock',
  'done': 'check-circle',
  'all': 'circle',

  // Actions
  'remove': 'x',
  'add': 'plus',
  'complete': 'check',
  'uncomplete': 'check',
  'postpone': 'clock',
  'newTask': 'arrow-right',
  'action': 'plus',
  'edit': 'edit-2'
}
