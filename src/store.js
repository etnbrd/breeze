import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    tasks: [
      {
        id: 1,
        title: 'Task Hola',
        content:'Some content about the task to do.',
        ack:false,
        parentId:0,
        parent:null,
        received: new Date(),
        estimated: 3,
        past: [],
        scheduled: [{
          key: '09/27/2017',
          date: new Date('09/27/2017'),
          duration: { HH:"03", mm:"03" },
          flexible: true,
          exclusive: false,
          time: undefined
        }]
      },{
        id: 2,
        title: 'Task Todo',
        content:'Some content about the task to do.',
        ack:false,
        parentId:0,
        parent:null,
        received: new Date(),
        estimated: 3,
        past: [],
        scheduled: []
      },{
        id: 3,
        title: 'Task Bene',
        content:'Some content about the task to do.',
        ack:false,
        parentId:0,
        parent:null,
        received: new Date(),
        estimated: 3,
        past: [],
        scheduled: []
      }
    ]
  },
  getters: {
    // TODO filtered task lists
    // TODO slot lists
    // https://vuex.vuejs.org/en/getters.html
  },
  mutations: {
    acknowledge (state, id) {
      state.tasks.find(t => t.id === id).ack = true;
    },
    unacknowledge (state, id) {
      state.tasks.find(t => t.id === id).ack = false;
    },
    create (state, task) {

      if (task.parentId) {
        task.parent = state.tasks.find(t => t.id === task.parentId)
      }

      task = Object.assign(task, {
        id: state.tasks.length + 1, // TODO fix this
        ack: false,
        parentId: task.parentId || 0,
        parent: task.parent || null,
        past: [],
        scheduled: []
      })

      state.tasks.unshift(task)
    },

    addSlots (state, {id, slots}) {
      var task = state.tasks.find(t => t.id === id)

      if (task) {
        slots.forEach(slot => {
          if (task.scheduled.some(s => s.date === slot.date)) {
            // TODO log warning : can't postpone twice the same day
          } else {
            task.scheduled.push(slot);
          }
        })
      } else {
        // TODO log error : task not found
      }
    },
    modifySlot(state, {id, slot, newSlot}) {
      var task = state.tasks.find(t => t.id === id)

      if (task) {
        task.scheduled.splice(task.scheduled.indexOf(slot), 1, newSlot);
      } else {
        // TODO log error : task not found
      }
    },
    removeSlots (state, {id, slots}) {
      var task = state.tasks.find(t => t.id === id)

      if (task) {
        slots.forEach(slot => {
          task.scheduled.splice(task.scheduled.indexOf(slot), 1);
        })
      } else {
        // TODO log error : task not found
      }
    }

    // createFrom (state, task) {
    //   var parent = state.tasks.find(t => t.id === task.id)
    //   // TODO avoid duplication in mutations
    //   task.id = state.tasks.length + 1; // TODO fix this
    //   task.ack = false;
    //   task.parent = parent;
    //   state.tasks.push(task)
    // }
  },
  actions: {
    // TODO make actions out of mutations
    // https://vuex.vuejs.org/en/actions.html
  }
})

export default store;
