import Vue from 'vue'
import VueTimepicker from 'vue2-timepicker';
import VueFeatherIcon from 'vue-feather-icon';

import App from './App.vue'
import store from './store.js'

import Navbar from './Navbar.vue';
import Task from './Task.vue';
import NewTask from './NewTask.vue';
import TaskList from './TaskList.vue';
import Calendar from './Calendar.vue';
import Slot from './Slot.vue';
import NewSlot from './NewSlot.vue';
import ListActionable from './ListActionable.vue';
import CalActionable from './CalActionable.vue';
import Date from './components/Date.vue';
import Button from './components/Button.vue';
import RadioButtons from './components/RadioButtons.vue';
import CheckButton from './components/CheckButton.vue';

Vue.use(VueFeatherIcon);
Vue.component('Timepicker', VueTimepicker);
Vue.component('Navbar', Navbar);
Vue.component('Task', Task);
Vue.component('NewTask', NewTask);
Vue.component('TaskList', TaskList);
Vue.component('Calendar', Calendar);
Vue.component('Slot', Slot);
Vue.component('NewSlot', NewSlot);
Vue.component('ListActionable', ListActionable);
Vue.component('CalActionable', CalActionable);
Vue.component('Date', Date);
Vue.component('Button', Button);
Vue.component('RadioButtons', RadioButtons);
Vue.component('CheckButton', CheckButton);

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
